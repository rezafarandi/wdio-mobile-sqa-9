// const { expect } = require('mocha');

describe('Native Demo App', () => {
    it('Sign Up', async () => {
      const loginButton = await $("~Login");
      await loginButton.click();

      const signUpTab = await $("~button-sign-up-container");
      await signUpTab.click();

      // Generate a random string for the email
      const randomString = Math.random().toString(36).substring(2, 8);

      // Create the email address by combining the random string with a domain
      const randomEmail = `rezamenjajal${randomString}@gmail.com`;

      const emailInput = await $("~input-email");
      await emailInput.setValue(randomEmail);
      await driver.pause(1000);

      const passwordInput = await $("~input-password");
      await passwordInput.setValue("Akuntes1");
      await driver.pause(1000);

      const confirmPasswordInput = await $("~input-repeat-password");
      await confirmPasswordInput.setValue("Akuntes1");
      await driver.pause(1000);

      const signUpButton = await $("~button-SIGN UP");
      await signUpButton.click();
      await driver.pause(2500);

      const signedUpPopup = await $("~android:id/parentPanel");
      const isPopupDisplayed = await signedUpPopup.isDisplayed();
      if (isPopupDisplayed) {
        // Handle the pop-up
      }
      //lanjutkan
    });
});