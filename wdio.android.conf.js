exports.config = {
  // ====================
  // Appium Configuration
  // ====================
  services: [
    [
      "appium",
      {
        command: "appium",
      },
    ],
  ],
  port: 4723,
  // ====================
  // Test Configurations
  // ====================
  specs: ["./test/specs/**/*.js"],
  capabilities: [
    {
      "platformName": "Android",
      "appium:automationName": "UiAutomator2",
      "appium:app":
        "/Users/RE4223/wdio-mobile-sqa-9/Android-NativeDemoApp-0.4.0.apk",
      "appium:udid": "emulator-5554",
      "appium:appWaitActivity": "*",
      "appium:appGrantPermissions": "true"
    },
  ],
  // ====================
  // Framework Configuration
  // ====================
  framework: "mocha",
  mochaOpts: {
    timeout: 60000,
  },
};